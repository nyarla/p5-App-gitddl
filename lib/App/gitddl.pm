package App::gitddl;

use strict;
use warnings;

our $VERSION = "0.0.1";

use Cwd qw/ getcwd /;
use JSON qw/ encode_json decode_json /;
use IO::Prompt::Simple qw/ prompt /;

use File::Spec;
use GitDDL;

sub run {
    my $class   = shift;
    local @ARGV = @_;

    my $command = shift @ARGV;
    
    if ( ! $command || $command eq '--help' ) {
        return $class->cmd_help();
    }

    my $cmd_name = "cmd_${command}";
    my $action   = $class->can($cmd_name);

    if ( ref $action ne 'CODE' ) {
        return $class->cmd_notfound($cmd_name);
    }

    return $action->( $class, @ARGV);
}

sub cmd_help {
    my $class = shift;
    print <<"...";
gitddl - Command line tool for GitDDL

    init        Initialize gitddl command.
    deploy      Deploy DDL file to database.
    upgrade     Upgrade database schema to database.
    help        Display this ;-)
...

    return 1;
}

sub cmd_notfound {
    my $class = shift;
    my $name  = shift;
    
    print "Command (${name}) is not defined in App::gitddl\n";
    return $class->cmd_help();
}

sub cmd_init {
    my $class = shift;
    local @ARGV = @_;

    my $workdir = getcwd();
    my $ddldef = File::Spec->catfile( $workdir, '.gitddl.json' );

    if ( -e $ddldef ) {
        warn ".gitddl.json already exists!\n";
        return 1;
    }

    my $worktree = prompt "GitDDL working tree: ", $workdir;
    my $ddl_file = prompt "SQL DDL file:", File::Spec->catfile( $workdir, 'db', 'sqlite.sql' );
    my $dsn_str  = prompt "SQL DSN: ", "dbi:SQLite:dbname=:memory:";

    my $data     = {
        work_tree   => $worktree,
        ddl_file    => $ddl_file,
        dsn         => $dsn_str,
    };

    open( my $fh, '>', $ddldef, )
        or die "Failed to open file: ${ddldef}: ${!}\n";
    print $fh encode_json($data);
    close($fh);

    return 0;
}

sub cmd_deploy {
    my $class = shift;
    local @ARGV = @_;

    my $migrator = $class->migrator;
       $migrator->deploy;

    return 0;
}

sub cmd_upgrade {
    my $class   = shift;
    local @ARGV = @_;

    my $migrator = $class->migrator;
       $migrator->upgrade_database;

    return 0;
}

sub migrator {
    my $class = shift;

    my $workdir = getcwd();
    my $ddldef  = File::Spec->catfile( $workdir, '.gitddl.json' );

    if ( ! -e $ddldef ) {
        die ".gitddl.json does not exists!\n";
        return 1;
    }

    open( my $fh, '<', $ddldef,)
        or die "Failed to open file: ${ddldef}: ${!}\n";
    my $json = do { local $/; <$fh> };
    close($fh);

    my $data = decode_json($json);
    my $migrator = GitDDL->new($data);

    return $migrator;
}


1;
__END__

=encoding utf-8

=head1 NAME

App::gitddl - Command line tool for L<GitDDL>

=head1 LICENSE

Copyright (C) 2014 Naoki OKAMURA (Nyarla).

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 AUTHOR

Naoki OKAMURA (Nyarla) E<lt>nyarla@thotep.netE<gt>

=cut

