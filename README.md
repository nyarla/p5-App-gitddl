# NAME

App::gitddl - Command line tool for [GitDDL](https://metacpan.org/pod/GitDDL)

# LICENSE

Copyright (C) 2014 Naoki OKAMURA (Nyarla).

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>
