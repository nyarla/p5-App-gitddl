requires 'Cwd';
requires 'JSON';
requires 'IO::Prompt::Simple';
requires 'GitDDL';

on 'test' => sub {
    requires 'Test::More', '0.98';
};

